var array = new Array(); // I create a new array
var sum=0; // Variable for the sum of each lines
var average=0; // variable for the average
var size=parseInt(prompt("size of the array? (enter 1 number and it will be an array of dimension n*n")) // The dimension of the array...

for(var i=0; i<size; i++)
{
	array[i] = new Array(); // I put arrays in arrays to have multiple dimensions, could be 8*8 or 80*80 or 2*2
}
for(var i=0; i<size; i++)
{
	for(var j=0; j<size; j++)
		array[i][j] = Math.floor((Math.random()*500)+1); // It would be too annoying to enter manually all the numbers via a prompt so I generate randoms numbers like in the 2nd exercise
}
for(var i=0; i<size; i++)
{
	document.write("<BR>");
	for(var j=0; j<size; j++)
		document.write(array[i][j]," - "); // I print the array on the screen
}
for(var i=0; i<size; i++)
{
	for(var j=0; j<size; j++)
	{
		sum=sum+array[i][j]; // I go thru the array and I sum all the lines, like line 1 is array[1][1]+array[2][1] etc.
	}
	average=sum/(size+1); // I return the average
	document.write("<BR>","Average ",i+1,": ",average); // print the result
	sum=0;
}