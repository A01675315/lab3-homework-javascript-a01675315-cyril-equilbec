number = prompt("Enter an integer:") // I thought to use the fact that the prompt returns a string
// So the iteger you enter is in fact a string.
var array = number.split("");
// I transform the string into an array to use the reverse() method
array.reverse() // ^
document.write("The reversed number is: ")
document.write(array.join('')) // I print the values of the array with a join so I have no commas and a proper number on screen.

